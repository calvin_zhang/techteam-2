#!/bin/bash

#Author:  Berry Sizemore
#Company:  GreaterGood.com
#Init:  Sun Mar 07 11:21:19 PST 2016
#Version:  .01

#Set the path so this will run from ~.
PATH=$PATH:$HOME/bin
export PATH

#Get the latest With Logging
echo `date` >> ~/log/shuttlePostfixEtc.sh.log

#Copy from repo to local filesystem
cp -a /home/berry/repos/techteam/postfix/etc/* /home/berry/tmp/postfix/ >> ~/log/shuttlePostfixEtc.sh.log
#Change the owner to root.
chown -R root.root /home/berry/tmp/postfix/* >> ~/log/shuttlePostfixEtc.sh.log
#Copy files from tmp dir to local dir.
cp -a /home/berry/tmp/postfix/* /etc/postfix/ >> ~/log/shuttlePostfixEtc.sh.log
#Remove the files that are no longer need.
rm -Rf /home/berry/tmp/postfix/* >> ~/log/shuttlePostfixEtc.sh.log
